import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  cards = [
    {
      titulo: "Moto Ducati",
      subtitulo: "R$70.000.00",
      conteudo: "Dimensões compactas, chassi ágil, equipamentos eletrônicos de ponta como modos de pilotagem, DTC (Ducati Traction Control), ABS de cruvas, Ducati Quick Shift Up/Down e um motor imponente com 147 CV e 12,4 Kgf. m de torque fazem da Monster um modelo com DNA da Ducati, repleto de esportividade, diversão e segurança.",
      foto: "https://www.motonline.com.br/noticia/wp-content/uploads/2021/07/ducati-superleggera-moto-mais-cara-2-800x420.jpg"
    },
    {
      titulo: "Moto BMW",
      subtitulo: "R$60.000.00",
      conteudo: "Eleita a melhor motocicleta aventureira do Brasil na premiação Moto do Ano 2020, agora com novo motor boxer e o sistema BMW ShiftCam que oferecem uma excepcional dinâmica de condução.",
      foto: "https://garagem360.com.br/wp-content/uploads/2021/07/2020-BMW-K1600-GT-Side-View.jpg"
    },
    {
      titulo: "Moto Futurista",
      subtitulo: "R$80.000.00",
      conteudo: "A moto elétrica Suprema é considerada um ciclomotor e destinada ao uso urbano, não é indicada para pessoas que percorrem longas distâncias, a autonomia da bateria é de até 70km, dependendo do trajeto percorrido e também do peso do condutor, esta é uma distância suficiente para as tarefas do dia a dia.",
      foto: "https://i0.statig.com.br/bancodeimagens/6p/1l/w8/6p1lw8y1rg2ys92z21xvyp83k.jpg"
    },
    {
      titulo: "Moto Yamaha Mt",
      subtitulo: "R$100.000.00",
      conteudo: "Com 2,090 m de comprimento, 0,755 m de largura, 1,070 m de altura e 1,380 m de entre-eixos, a Yamaha MT 03 2022 veste bem. Ela tem 169 kg, tem 0,780 m de altura do assento do piloto e 160 mm de altura livre do solo.",
      foto: "https://www.motoo.com.br/fotos/2021/10/960_720/yamaha_mt-03_2022_1_13102021_41880_960_720.jpg"
    }
  ]
  constructor() {}

}
